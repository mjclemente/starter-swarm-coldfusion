<h1>Starter Swarm Template - ColdFusion</h1>
<h2>Hello, we're in the container!</h2>
<pre>

         \\\///
        / _  _ \
      (| (.)(.) |)
.---.OOOo--()--oOOO.---.
|                      |
|     Hello World!     |
|                      |
'---.oooO--------------'
     (   )   Oooo.
      \ (    (   )
       \_)    ) /
             (_/

</pre>

</p>
<cfscript>
writeOutput( "<p>What time is it CFSummit? <i>#timeformat( now(), 'short' )#?</i></p><p><b>Sounds like it's container time.</b></p>" );
</cfscript>