# Starter Swarm Template - ColdFusion <!-- omit in toc -->

The goal of this project is to provide a basic template for beginning to use and understand Docker Swarm in the context of an Adobe ColdFusion application. It provides a pattern and structure for working with:

- Matching Local and Production development environments
- Use of a private registry
- CI/CD pipeline for building and deploying to your Swarm
- Use of environment variables
- Management of sensitive information via secrets
- An approach to managing/adding/rotating secrets

Container orchestration in production requires a lot more than this (certs, load balancers, monitoring, etc), but this framework should provide a ColdFusion-focused foundation for understanding, developing, and deploying with Docker Swarm.

*Note that this project is specifically tailored for Adobe ColdFusion. If you're interested in using Lucee CFML with Docker Swarm, check out this project:* [`starter-swarm-cfml`](https://gitlab.com/mjclemente/starter-swarm-cfml)

## Table of Contents <!-- omit in toc -->
- [Prerequisites](#prerequisites)
- [Project Steps](#project-steps)
  - [Initial repository configuration](#initial-repository-configuration)
  - [Local Development](#local-development)
  - [Setting up your Gitlab deployment SSH key](#setting-up-your-gitlab-deployment-ssh-key)
  - [Creating the Swarm](#creating-the-swarm)
  - [Verifying the SSH host keys](#verifying-the-ssh-host-keys)
  - [Secret Management](#secret-management)
  - [Deployment (CI/CD)](#deployment-cicd)

## Prerequisites

Before getting started with this project, there are a few things that you'll need to have in place.

- **Gitlab account**: We'll host the repository with Gitlab, so that we can use their integrated container registry and CI/CD pipeline. It's really nice to have everything in one place. It's even nicer when it's free. [Register for Gitlab here](https://gitlab.com/users/sign_in#register-pane).
- **DigitalOcean account**: We need somewhere to host and deploy our Swarm. While the code here should work in the context of any cloud provider, I'll be using DigitalOcean in my instructions and examples. [Create a DigitalOcean account here](https://m.do.co/c/8acbd6928587) ($50 referral credit).
- **Docker for Mac/Windows**: Along with local development, we'll leverage the functionality added in Docker 18.09 to manage our remote Docker hosts from our local machine. [Get Docker Desktop here](https://www.docker.com/products/docker-desktop).

## Project Steps

### Initial repository configuration

1. First, after logging into your Gitlab account, you'll need to fork this repository using the Gitlab.com interface. This copies it to your Gitlab account, where we can configure the private image registry and CI/CD operations.

2. Clone your copy of the repository and navigate into it (If you haven't [set up an SSH key for your Gitlab account](https://docs.gitlab.com/ee/gitlab-basics/create-your-ssh-keys.html), you'll need to do that first).

    ```bash
    git clone git@gitlab.com:YOUR-USERNAME/starter-swarm-coldfusion.git
    cd starter-swarm-coldfusion
    ```

3. In the project root, make a copy of `.env.example` and name it `.env`. Within the `.env` file, replace my Gitlab namespace, `mjclemente`, with your own.

4. The next step will be to create the files that will hold your administrator passwords. These will not be in version control and will be provided to the Docker image as secrets (more on that later). For now, you just need to generate them, so in the project root, run:
   
   ```bash
   mkdir .secrets
    echo "commandbox" > .secrets/cfml.admin.password.dev
    echo $(openssl rand -base64 20) > .secrets/cfml.admin.password.v1
   ```

We'll be circling back to Gitlab account configuration, but before we do, we need to set up a deployment SSH key to use.

### Local Development

You'll be able to develop locally by running `docker-compose up` from the root of the project. This will spin up a CommandBox server and the project will be served on [http://127.0.0.1:8080/](http://127.0.0.1:8080/). Changes you make will be reflected immediately. Because it's local, we leave the ColdFusion Administrator accessible at [http://127.0.0.1:8080/CFIDE/administrator](http://127.0.0.1:8080/CFIDE/administrator), with the password generated earlier (commandbox).

Once your ready to deploy, we can move on.

### Setting up your Gitlab deployment SSH key

1. We're going to set up a new deployment SSH key, just for Gitlab. This is separate from the primary SSH key that you use for your Gitlab account. Gitlab's CI/CD jobs will use this key to deploy to the Swarm on your DigitalOcean servers (don't worry, we'll get around to setting them up too).
   1. In your terminal, run the command to create and name an SSH key:

      ```bash
      ssh-keygen -t ed25519 -C "Gitlab Deployment Key"
      ```

      For the sake of organization, we'll use the same name for this key when adding it to Gitlab and DigitalOcean later on.

   2. When prompted, provide a custom location for it to be stored - remember, you're not going to be using this as your default SSH key - it's just for Gitlab deployment. For mine, I used: `/Users/myuser/.ssh/gitlab_com_rsa`

   3. When prompted for a passphrase, **leave it blank**.

   4. This will create two files for you, the public and the private:
      - `Users/youruser/.ssh/gitlab_com_rsa`
      - `Users/youruser/.ssh/gitlab_com_rsa.pub `

   5. Add the public key to your DigitalOcean account. This way we can make sure it's added to Droplets that we will be creating for the Swarm. ([DO Guide](https://www.digitalocean.com/docs/droplets/how-to/add-ssh-keys/to-account/))

   6. Next, add the same public key to the Gitlab repository as a [deploy key](https://docs.gitlab.com/ee/ssh/README.html#per-repository-deploy-keys). Within the repository, navigate to **Settings > Repository > Deploy Keys**. Here, you'll add the **public key**. It does not need write access.

   7. Finally, we're going to configure the private key. Within the repository's **Settings > CI/CD > Variables**, create a new *protected* variable. For the Input variable key, enter `SSH_PRIVATE_KEY` and in the Value field paste the content of the private key that you created. You won't be able to mask these variables. Check the option so that it's protected and then save it. Once we set up our Swarm, we'll circle back to these environment variables to add our known hosts.

### Creating the Swarm

We're going to need to create a new Swarm. I put a script together that handles the entire process for you: [Swarm Creation Script](https://github.com/mjclemente/do-swarm-create). It uses [`doctl`](https://www.digitalocean.com/community/tutorials/how-to-use-doctl-the-official-digitalocean-command-line-client), DigitalOcean's command line tool, to do most of the work.

If you run that script with the default settings, it will spin up three Docker hosts, add your SSH keys, and init a Swarm that you'll be able to manage from your local command line. Set the environment variable `DO_MANAGER_COUNT=1` if you only want to deal with one host to begin with.

If you're going to use an existing Swarm, be sure to add your SSH key and the Gitlab Deployment SSH key to the Droplets.

### Verifying the SSH host keys

For optimal security, we will also [verify the SSH host keys](https://docs.gitlab.com/ee/ci/ssh_keys/#verifying-the-ssh-host-keys) of the manager node that we'll be deploying to. This is done by scanning them and adding them as a CI/CD environment variable as well.

1. From a trusted network (such as on the host itself), we'll scan the Droplet's public IP:

    ```bash
    ssh-keyscan 1.2.3.4
    ```

2. Take the output of this command and again navigate to the repository's **Settings > CI/CD > Variables**. As with the `SSH_PRIVATE_KEY`, we'll add this as a second protected environment variable with the key `SSH_KNOWN_HOSTS`.

3. Protected environment variables are only made available to protected branches, so we need to update the setting for our deployment branch. Navigate to **Settings > Repository > Protected Branches**. In the Branch field, select `deploy`.  Restrict merge and push permissions to Maintainers, then click Protect to apply the settings.

### Secret Management

Secrets must be created on the Swarm before you can deploy your application to it. This is because the `CF_ADMINPASSWORD` environment variable depends on a secret; it can't be used until the secret is set.

In order to deploy the secrets to the Swarm, we'll use the script `./build/deploy-secrets.sh`. When you run this script it will prompt you for SSH credentials to your Droplet. You can get the IP of the Droplet from your DigitalOcean account and you should be able to use `root@dropletIP`. This is needed in order to SSH into the Swarm to create the secrets. The script provides an explanation of what it does.

### Deployment (CI/CD)